'''
Created on 14/09/2014

@author: mario
'''
from os import system
system("clear")

import sys
import moduleLog
import database

def leitura():
    """
    Faz tratamento de leitura do leitor de código de barras
    retorna:
        0 se valor de saida ('e') da função for inserido
        senão retorna o código lido
    """
    leitura = input('Entre com o código do participante ou "e" para sair: ')
    #proximo input pega o enter que sobra do leitor
    input()

    if leitura == 'e':
        return 0
    return leitura

def palestra():
    """
    Faz cadastro de frequencia nas palestras
    """
    #cria o moduleLog
    log = moduleLog.Log('palestras')
    #cria conexão com o banco
    db = database.DataBase()

    #define qual será a palestra
    print(*db.pesquisa_todas_palestras(), sep="\n")
    palestra=0
    while not palestra:
        cod_palestra = input("\nDigite o código da palestra: ")
        palestra = db.pesquisa_palestra(cod_palestra)

    print("Palestra escolhida: ", palestra, "\n\n\n\n")

    while True:
        #leitura do leitor
        cod_participante = leitura()

        #condicional para saida do modulo de cadastro
        if cod_participante == 0:
            print('saindo do modulo de cadastro de participantes em palestras...')
            break

        #adiciona valores no arquivo de log
        log.adiciona(cod_participante, cod_palestra)
        #cria objeto para adicionar ao banco
        obj = database.PresencaPalestra(cod_palestra, cod_participante)
        #adiciona obj ao banco
        db.adiciona(obj)

def minicurso():
    #cria o moduleLog
    log = moduleLog.Log('minicursos')
    #cria conexão com o banco
    db = database.DataBase()

    #pega a escolha do minicurso
    print(*db.pesquisa_todos_minicursos(), sep="\n")
    cod_minicurso = 0
    while not cod_minicurso:
        cod_minicurso = input("\nDigite o código do minicurso: ")
        minicurso = db.pesquisa_minicurso(cod_minicurso)

    print("Minicurso escolhido: ", minicurso, "\n\n\n\n")

    while True:
        #leitura do leitor
        cod_participante = leitura()

        #condicional para saida do modulo de cadastro
        if cod_participante == 0:
            print('saindo do modulo de cadastro de participantes em palestras...')
            break

        #adiciona valores no arquivo de log
        log.adiciona(cod_minicurso, cod_minicurso)
        #cria objeto para adicionar ao banco
        obj = database.PresencaMinicurso(cod_minicurso, cod_participante)
        #adiciona obj ao banco
        db.adiciona(obj)

####################################

def main():
    while True:
        print("Escolha opção de operação:")
        print("1 - cadastro frequencia palestras ")
        print("2 - cadastro frequencia minicurso")
        print("0 - exit")
        options = {0 : sys.exit,
                   1 : palestra,
                   2 : minicurso}

        escolha = int(input('Opção: '))
        options[escolha]()
        system("clear")

if __name__ == '__main__':
    main()