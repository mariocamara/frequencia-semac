'''
Created on 14/09/2014

@author: mario
'''

import os
from datetime import date

class Log(object):
    '''
    Salva todos os dados em arquivo para caso haja necessidade
    '''


    def __init__(self, tipo):
        """
        cria a pasta se ela não existe ainda e seu arquivo log
        """

        #local que esta o programa, onde ele foi chamado
        dir_chamado = os.getcwd()
        pasta_log = dir_chamado+"/"+tipo
        if not os.path.exists(pasta_log):
            os.makedirs(pasta_log)

        self.nome_arquivo = pasta_log + '/' + str(date.today().day) + '.log'
        if not os.path.exists(self.nome_arquivo):
            arquivo = open(self.nome_arquivo, 'w')
            arquivo.write('Arquivo log com informações dos participantes\n')
            arquivo.write('No participante  -  No palestra\n')

    def adiciona(self, participante, palestra):
        participante = str(participante)
        palestra = str(palestra)
        with open(self.nome_arquivo, 'a') as arquivo:
            arquivo.write(participante + " " + palestra)
            arquivo.write('\n')
