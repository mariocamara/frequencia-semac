'''
Created on 14/09/2014

Dependencia de pacotes:
        python-psycopg2

@author: mario
'''

_nome_banco = "BaseSemac"

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Column, Integer, Boolean, String

import sqlalchemy

Base = declarative_base()

##########################################################################

class PresencaPalestra(Base):
    __tablename__= 'tb_r_presenca_palestra'

    id_palestra = Column(Integer, primary_key=True)
    id_participante = Column(Integer, primary_key=True)
    presenca = Column(Boolean)

    def __init__(self, id_palestra, id_participante):
        self.id_palestra = id_palestra
        self.id_participante = id_participante
        self.presenca = True

##########################################################################

class Participantes(Base):
    __tablename__='tb_participantes'

    id_participante = Column(Integer, primary_key=True) 
    nome = Column(String(100))
    cpf = Column(String(15))
    situacao_pagamento = Column(String(50))

##########################################################################

class Palestras(Base):
    __tablename__='tb_palestras'
    
    id_palestra = Column(Integer,primary_key=True)
    id_palestrante = Column(Integer)
    assunto = Column(String(100))
    
    def __repr__(self):
        return '<Palestra (id: {}, assunto: {})>'.format(self.id_palestra, self.assunto)

##########################################################################

class Minicurso(Base):
    __tablename__='tb_minicursos'
    
    id_minicurso = Column(Integer, primary_key=True)
    id_palestrante = Column(Integer)
    nome = Column(String)

    def __init__(self, id_minicurso, id_palestrante):
        self.id_minicurso = id_minicurso
        self.id_palestrante = id_palestrante
        self.presenca = True

    def __repr__(self):
        return '<Minicurso (id: {}, nome: {})>'.format(self.id_minicurso, self.nome)

##########################################################################

class PresencaMinicurso(Base):
    __tablename__='tb_r_presenca_minicursos'

    id_minicurso = Column(Integer, primary_key=True)
    id_participante = Column(Integer)
    presenca = Column(Boolean)

    def __init__(self, id_minicurso, id_participante):
        self.id_minicurso = id_minicurso
        self.id_participante = id_participante
        self.presenca = True


##########################################################################

class DataBase(object):
    '''
    classdocs
    '''

    #some_engine = create_engine('postgresql://scott:tiger@localhost/')
    def __init__(self):
        engine = create_engine('postgresql+psycopg2://postgres:mario@localhost/'+_nome_banco, echo = False)
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def adiciona(self, obj):
        try:
            self.session.add(obj)
            self.session.commit()
        except sqlalchemy.exc.IntegrityError as err:
            print("\n\n\nAlgum erro aconteceu de integridade no banco.")
            print("O erro do SGBD está descrito abaixo:\n")
            print(err)
            print("\n\n\n\n>try again, se der erro de novo peça ajuda aos deuses\n")
            self.session.rollback()
        except:
            print("Eita porra!\n Deu errado alguma coisa ai, vc deve ta fazendo merda")
            self.session.rollback()

    def pesquisa_participante(self):
        pass

    def valida_participante(self, presenca_palestra):
        #valida pagamento
        #pesquisa no banco o participante
        participante = self.session.query(Participantes)
        participante = participante.filter(Participantes.id_participante == presenca_palestra.id_participante)

        try:
            participante = participante.one()
            if (participante.situacao_pagamento == "confirmado"): #FIXME ver o que foi colocado como parametro
                pass #retornar validação correta
        except:
            print("participante não encontrado")

    def pesquisa_palestra(self, id_palestra):
        palestra = self.session.query(Palestras).filter(Palestras.id_palestra == id_palestra)
        try:
            return palestra.one()
        except:
            return 0
    
    def pesquisa_minicurso(self, id_minicurso):
        minicurso = self.session.query(Minicurso).filter(Minicurso.id_minicurso == id_minicurso)
        try:
            return minicurso.one()
        except:
            return 0

    def pesquisa_todos_minicursos(self):
        return self.session.query(Minicurso).all()

    def pesquisa_todas_palestras(self):
        return self.session.query(Palestras).all()